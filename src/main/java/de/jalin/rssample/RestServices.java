package de.jalin.rssample;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class RestServices extends Application {

    private Set<Object> singletons = new HashSet<Object>();

    public RestServices() {
         singletons.add(new TalkService());
    }

    public Set<Object> getSingletons() {
        return singletons;
    }
}