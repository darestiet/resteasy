package de.jalin.rssample;

import java.util.ArrayList;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/talk")
public class TalkService {

	@GET
	@Path("/chat/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Chat loadChat(@PathParam("name") String name) {
		Chat chat = new Chat();
		chat.name = name;
		chat.history = new ArrayList<>();
		Message msg1 = new Message();
		msg1.author = "anon";
		msg1.date = System.currentTimeMillis();
		msg1.text = "Hallo Welt!";
		chat.history.add(msg1);
		Message msg2 = new Message();
		msg2.author = "anon";
		msg2.date = System.currentTimeMillis();
		msg2.text = "Ja, hallo schöne Welt!";
		chat.history.add(msg2);
		return chat;
	}
	
}
